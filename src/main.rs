use term;
use term::util::{FgColor, Text};
use time::{Date, Duration, Month, OffsetDateTime, Weekday};

use std::collections::HashMap;
use std::path::Path;
use std::process::{Command, Stdio};

use clap::Parser;

#[derive(Debug, Parser)]
struct Args {
    #[arg(short, long = "author", value_name = "AUTHOR")]
    authors: Vec<String>,
    #[arg(short, long = "repository", value_name = "REPOSITORY")]
    repositories: Vec<String>,
    #[arg(short, long, value_parser=clap::value_parser!(u16), default_value="53")]
    weeks: u16,
    #[arg(short, long, default_value="gh", value_parser=["gh", "fire", "isolum", "blues", "bmy", "bmw"])]
    color: String,
}

#[allow(dead_code)]
#[derive(Clone, Debug)]
struct Commit {
    hash: String,
    date: Date,
}

fn get_month(m: u8) -> Result<Month, String> {
    if m < 1 || m > 12 {
        return Err("out of range month".to_string());
    }

    let month = match m {
        1 => Month::January,
        2 => Month::February,
        3 => Month::March,
        4 => Month::April,
        5 => Month::May,
        6 => Month::June,
        7 => Month::July,
        8 => Month::August,
        9 => Month::September,
        10 => Month::October,
        11 => Month::November,
        12 => Month::December,
        _ => unreachable!(),
    };

    Ok(month)
}

fn get_weekday_value(weekday: Weekday, first_day_of_week: Weekday) -> u8 {
    let weekday_value = match weekday {
        Weekday::Monday => 0,
        Weekday::Tuesday => 1,
        Weekday::Wednesday => 2,
        Weekday::Thursday => 3,
        Weekday::Friday => 4,
        Weekday::Saturday => 5,
        Weekday::Sunday => 6,
    };

    // faut faire + 1 mod something

    let value: u8 = match first_day_of_week {
        Weekday::Monday => weekday_value,
        Weekday::Tuesday => weekday_value + 6,
        Weekday::Wednesday => weekday_value + 5,
        Weekday::Thursday => weekday_value + 4,
        Weekday::Friday => weekday_value + 3,
        Weekday::Saturday => weekday_value + 2,
        Weekday::Sunday => weekday_value + 1,
    };

    value % 7
}

fn get_commits(repos: Vec<String>, authors: Vec<String>) -> Vec<Commit> {
    let mut commits: Vec<Commit> = Vec::new();

    for repo in repos {
        let repo_path = Path::new(&repo);

        for author in &authors {
            let git_args = vec![
                "-P".to_string(),
                "-C".to_string(),
                format!("{}", repo_path.display()),
                "log".to_string(),
                format!("--author={}", author),
                "--pretty=%h %ad".to_string(),
                "--date=format:%Y/%m/%d".to_string(),
            ];

            let git = Command::new("git")
                .args(git_args)
                .stdout(Stdio::piped())
                .stderr(Stdio::null())
                .spawn()
                .expect("Error: failed to run `git`");

            let git_output = git
                .wait_with_output()
                .expect("Error: failed to wait on `git`");

            let decoded_output = String::from_utf8_lossy(&git_output.stdout);

            for line in decoded_output.lines() {
                let (hash, date): (String, Date) = {
                    let split_line: Vec<&str> = line.split_whitespace().collect();
                    let hash = split_line.get(0).unwrap().to_string();

                    let date: Date = {
                        let split_date: Vec<&str> = split_line.get(1).unwrap().split("/").collect();
                        let year: i32 = split_date.get(0).unwrap().parse::<i32>().unwrap();
                        let month: Month =
                            get_month(split_date.get(1).unwrap().parse::<u8>().unwrap()).unwrap();
                        let day: u8 = split_date.get(2).unwrap().parse::<u8>().unwrap();

                        Date::from_calendar_date(year, month, day).unwrap()
                    };

                    (hash, date)
                };

                let commit = Commit {
                    hash: hash,
                    date: date,
                };

                commits.push(commit);
            }
        }
    }

    commits
}

fn get_commits_per_date(commits: Vec<Commit>) -> HashMap<Date, u32> {
    let mut commits_per_day: HashMap<Date, u32> = HashMap::new();

    for commit in commits {
        let date = commit.date;

        commits_per_day
            .entry(date)
            .and_modify(|counter| *counter += 1)
            .or_insert(1);
    }

    commits_per_day
}

fn get_nbr_of_commits(
    commits_per_date: &HashMap<Date, u32>,
    start_date: Date,
    end_date: Date,
) -> u32 {
    let mut nbr_of_commits = 0;
    let mut date_iter = start_date;

    while date_iter <= end_date {
        nbr_of_commits += *commits_per_date.get(&date_iter).unwrap_or(&0);
        date_iter += Duration::DAY;
    }

    nbr_of_commits
}

fn get_start_date(end_date: Date, nbr_of_weeks: u16, start_week_day: Weekday) -> Date {
    // wtf am i doing............ first get the monday of the current week, then
    // remove the number of weeks we have, minus one for the current week
    let start_date = (end_date
        - (get_weekday_value(end_date.weekday(), start_week_day) * Duration::DAY))
        - (Duration::WEEK * (nbr_of_weeks - 1));

    start_date
}

fn get_contribution_color(nbr_of_commit: u32, colormap: &str) -> FgColor {
    // Color Maps from https://github.com/holoviz/colorcet
    let cm_gh = [
        FgColor::RGB(0xeb, 0xed, 0xf0),
        FgColor::RGB(0x9b, 0xe9, 0xa8),
        FgColor::RGB(0x40, 0xc4, 0x63),
        FgColor::RGB(0x30, 0xa1, 0x4e),
        FgColor::RGB(0x21, 0x6e, 0x39),
    ];
    let cm_fire = [
        FgColor::RGB(0xff, 0xc4, 0x04),
        FgColor::RGB(0xfe, 0x79, 0x00),
        FgColor::RGB(0xea, 0x11, 0x00),
        FgColor::RGB(0x9f, 0x04, 0x00),
        FgColor::RGB(0x59, 0x01, 0x00),
    ];
    let cm_isolum = [
        FgColor::RGB(0xe8, 0xc0, 0x75),
        FgColor::RGB(0xc3, 0xcc, 0x7d),
        FgColor::RGB(0x9a, 0xd4, 0x95),
        FgColor::RGB(0x7f, 0xd6, 0xbc),
        FgColor::RGB(0x74, 0xd4, 0xe0),
    ];
    let cm_blues = [
        FgColor::RGB(0xd0, 0xdc, 0xed),
        FgColor::RGB(0xb0, 0xc9, 0xe8),
        FgColor::RGB(0x94, 0xb5, 0xdc),
        FgColor::RGB(0x7d, 0xa1, 0xca),
        FgColor::RGB(0x63, 0x8e, 0xbb),
    ];
    let cm_bmy = [
        FgColor::RGB(0xff, 0xb5, 0x23),
        FgColor::RGB(0xff, 0x6d, 0x54),
        FgColor::RGB(0xee, 0x16, 0x75),
        FgColor::RGB(0xa6, 0x00, 0x8b),
        FgColor::RGB(0x34, 0x14, 0xa8),
    ];
    let cm_bmw = [
        FgColor::RGB(0xff, 0xa3, 0xff),
        FgColor::RGB(0xf8, 0x50, 0xff),
        FgColor::RGB(0xb3, 0x1e, 0xff),
        FgColor::RGB(0x18, 0x1f, 0xfd),
        FgColor::RGB(0x00, 0x13, 0xbf),
    ];
    // let _ = [
    //     FgColor::RGB(0x, 0x, 0x),
    //     FgColor::RGB(0x, 0x, 0x),
    //     FgColor::RGB(0x, 0x, 0x),
    //     FgColor::RGB(0x, 0x, 0x),
    //     FgColor::RGB(0x, 0x, 0x),
    // ];

    let cm: [FgColor; 5] = match colormap {
        "gh" => cm_gh,
        "fire" => cm_fire,
        "isolum" => cm_isolum,
        "blues" => cm_blues,
        "bmy" => cm_bmy,
        "bmw" => cm_bmw,
        _ => cm_gh,
    };

    match nbr_of_commit {
        0 => cm[0],
        1..=2 => cm[1],
        3..=4 => cm[2],
        5..=9 => cm[3],
        10.. => cm[4],
    }
}

fn git_activity_get_legend(colormap: &str) -> String {
    format!(
        "Less {}■{} {}■{} {}■{} {}■{} {}■{} More",
        get_contribution_color(0, colormap),
        Text::Reset,
        get_contribution_color(1, colormap),
        Text::Reset,
        get_contribution_color(3, colormap),
        Text::Reset,
        get_contribution_color(5, colormap),
        Text::Reset,
        get_contribution_color(10, colormap),
        Text::Reset,
    )
}

/// the function assume that the first line of git_activity is the start_week_day
fn git_activity_add_weekday(git_activity: &str, start_week_day: Weekday) -> String {
    let space_padding = 4;
    let short_weekday = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

    let mut weekday = start_week_day;
    let mut _s = String::with_capacity(git_activity.len() + 7 * space_padding);

    for line in git_activity.lines() {
        _s += match weekday {
            Weekday::Monday => format!("{} {}", short_weekday[0], line),
            Weekday::Wednesday => format!("{} {}", short_weekday[2], line),
            Weekday::Friday => format!("{} {}", short_weekday[4], line),
            _ => format!("{}{}", " ".repeat(space_padding), line),
        }
        .as_str();

        _s += "\n";

        weekday = weekday.next();
    }

    _s
}

fn git_activity_get_months(start_date: Date, end_date: Date) -> String {
    let months_short_str: [&str; 12] = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
    ];

    let mut date_iter = start_date;
    let mut last_month_printed: usize = 13;

    let mut months_line = String::new();

    while date_iter <= end_date {
        let month = date_iter.month() as usize;

        if month != last_month_printed {
            months_line += months_short_str[month - 1];

            last_month_printed = month;

            date_iter += Duration::WEEK * 3;
        } else {
            months_line += " ";
            date_iter += Duration::WEEK;
        }
    }

    months_line
}

fn box_it(s: &str, title: &str, width: usize) -> String {
    let mut _s = String::new();

    let title_width = title.chars().count();

    if title_width > width - 2 {
        _s = format!("{}\n", title);
        _s += format!("╭{}╮\n", "─".repeat(width)).as_str();
    } else {
        let line_width = width - title_width - 2 - 1;
        _s = format!("╭─ {} {}╮\n", title, "─".repeat(line_width));
    }

    for line in s.lines() {
        _s += format!("│{}│\n", line).as_str();
    }

    _s += format!("╰{}╯\n", "─".repeat(width)).as_str();

    _s
}

fn print_git_activity(
    start_date: Date,
    end_date: Date,
    commits_per_date: &HashMap<Date, u32>,
    show_weekend: bool,
    colormap: &str,
) {
    let mut date_iter = start_date;

    let print_weekday = true;
    let print_months = true;
    let print_border = true;
    let print_legend = true;

    let mut max_nbr_col = 0;

    while date_iter < end_date {
        max_nbr_col += 1;
        date_iter += Duration::WEEK;
    }

    let mut git_activity = String::with_capacity(max_nbr_col * 7);

    for i in 0..7 {
        let week_day: Date = start_date + (i * Duration::DAY);

        if !show_weekend
            && (week_day.weekday() == Weekday::Saturday || week_day.weekday() == Weekday::Sunday)
        {
            continue;
        }

        for col in 0..max_nbr_col {
            let date = week_day + (Duration::WEEK * (col as u32));

            if date <= end_date {
                let nbr_of_commits = *commits_per_date.get(&date).unwrap_or(&0);
                let color = get_contribution_color(nbr_of_commits, colormap);

                git_activity += format!("{}■{}", color, Text::Reset).as_str();
            } else {
                git_activity += " ";
            }
        }

        git_activity += "\n";
    }

    let mut width = max_nbr_col;

    if print_weekday {
        git_activity = git_activity_add_weekday(git_activity.as_str(), start_date.weekday());
        width += 4;
    }

    if print_months {
        let months = git_activity_get_months(start_date, end_date);
        git_activity = format!("{:>w$}\n{}", months, git_activity, w = width);
    }

    if print_legend {
        let legend = git_activity_get_legend(colormap);
        let legend_visible_size = 19;
        git_activity += format!(
            "{:>w$}",
            legend,
            w = legend.chars().count() + width - legend_visible_size
        )
        .as_str();
    }

    let nbr_of_commits = get_nbr_of_commits(&commits_per_date, start_date, end_date);

    if print_border {
        git_activity = box_it(
            git_activity.as_str(),
            format!("Number of commits: {}", nbr_of_commits).as_str(),
            width,
        );
    }

    print!("{}", git_activity);
}

fn main() {
    let args = Args::parse();

    let commits = get_commits(args.repositories, args.authors);
    let commits_per_date = get_commits_per_date(commits);

    let today = OffsetDateTime::now_utc();

    let start_date = get_start_date(today.date(), args.weeks, Weekday::Sunday);

    print_git_activity(
        start_date,
        today.date(),
        &commits_per_date,
        true,
        &args.color,
    );
}
